package main

import (
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo"
)

type Page struct {
	Path string    `json:"path"`
	Seen time.Time `json:"time"`
}

func GetPaths() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, generatePaths())
	}
}

func generatePaths() []Page {
	var paths []Page

	for i := 0; i < 20; i++ {
		domain := Domains[rand.Intn(len(Domains))]
		parts := strings.Split(domain, ".")

		path := Page{
			Path: "/" + parts[0],
			Seen: time.Now().Add(-time.Duration(int(time.Minute) * rand.Intn(2000))),
		}

		paths = append(paths, path)
	}

	return paths
}
