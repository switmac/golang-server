package main

import (
	"strconv"
	"net/http"

	"github.com/labstack/echo"
)

type Customer struct {
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	EmailAddress string `json:"email"`
	PhoneNumber  string `json:"phone"`
	Address      string `json:"address"`
	City         string `json:"city"`
	State        string `json:"state"`
	Zip5         string `json:"zip"`
}

func GetCustomers() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, generateCustomers())
	}
}

func generateCustomers() []Customer {
	var customers []Customer

  for i := 0; i < 20; i++ {

		customer := Customer{
      FirstName:    strconv.Itoa(i),
  		LastName:     "Lastname",
  		EmailAddress: "EmailAddress.com",
  		PhoneNumber:  "850-3467",
  		Address:      "Block 2 Lot 1",
  		City:         "California",
  		State:        "Los Angeles",
  		Zip5:         "90242",
		}

    customers = append(customers, customer)
	}

	return customers
}
