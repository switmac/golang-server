package main

import (
	"fmt"
	"math/rand"
	"time"
	"strconv"
	"net/http"

	"github.com/labstack/echo"
)

type Option struct{
	Property string    `json:"property"`
	Values   []string  `json:"values"`
}

type Filter struct {
	Name     string    `json:"name"`
	Options	 []Option	 `json:"options"`
	Created  time.Time `json:"time"`
	Id			 int			 `json:"id"`
}

func GetFilters() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, generateFilters())
	}
}


func CreateFilter() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, createFilter())
	}
}

func EditFilter() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, createFilter())
	}
}

func DeleteFilter() echo.HandlerFunc {
	return func(c echo.Context) error {

		return c.JSON(http.StatusOK, deleteFilter(c))
	}
}

func deleteFilter(c echo.Context) string{
	fmt.Println("id")
	return c.Param("id")
}

func createFilter() Filter{
	var options  []Option

	for j:=0; j < 3; j++ {
			property := Properties[rand.Intn(len(Properties))]
			values := []string{"Value1", "Value2", "Value3",}

			option := Option{
				Property : property,
				Values : values,
			}

			options = append(options, option)
	}

	filter := Filter{
		Name: "Filter",
		Options : options,
		Created: time.Now().Add(-time.Duration(int(time.Minute) * rand.Intn(2000))),
	}

	return filter
}

func editFilter() Filter{
	var options  []Option

	for j:=0; j < 3; j++ {
			property := Properties[rand.Intn(len(Properties))]
			values := []string{"Value1", "Value2", "Value3",}

			option := Option{
				Property : property,
				Values : values,
			}

			options = append(options, option)
	}

	filter := Filter{
		Name: "Filter",
		Options : options,
		Created: time.Now().Add(-time.Duration(int(time.Minute) * rand.Intn(2000))),
	}

	return filter
}

func generateFilters() []Filter {
	var filters []Filter

	for i := 0; i < 20; i++ {
		var options []Option

		for j:=0; j < 3; j++ {
				property := Properties[rand.Intn(len(Properties))]
				values := []string{"Value1", "Value2", "Value3",}

				option := Option{
					Property : property,
					Values : values,
				}

				options = append(options, option)
		}

		filter := Filter{
			Name: strconv.Itoa(i),
			Options : options,
			Created: time.Now().Add(-time.Duration(int(time.Minute) * rand.Intn(2000))),
			Id : i,
		}

		filters = append(filters, filter)
	}

	return filters
}

var (
	Properties = []string{
		"City",
		"IPAddress",
		"ISP",
	}
)
