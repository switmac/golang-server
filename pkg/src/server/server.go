// @APIVersion 1.0.0
// @APITitle Traffic_Gate_API
// @APIDescription Traffic Gate API for your website traffic monitoring and handling.
// @Contact counterstrike@contact.me
// @TermsOfServiceUrl http://google.com/
// @License BSD
// @LicenseUrl http://opensource.org/licenses/BSD-2-Clause

package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
	"github.com/rs/cors"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(standard.WrapMiddleware(cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:8080", "http://localhost:9000"},
		AllowedMethods: []string{"PUT", "DELETE", "GET", "POST"},
	}).Handler))
	
	// Routes
	e.Get("/sites", GetSites())
	e.Post("/sites/save", SaveSiteAction())
	e.Get("/paths/:host", GetPaths())
	e.Get("/gates/:user", GetFilters())
	e.Post("/gates", CreateFilter())
	e.Put("/gates/:id", EditFilter())
	e.Delete("/gates/:id", DeleteFilter())
	e.Get("/customers", GetCustomers())

	// Start server
	e.Run(standard.New(":1323"))
	return
}
